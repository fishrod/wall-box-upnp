//
//  WallBoxDLNAManager.h
//  Wall Box
//
//  Created by Gavin Williams on 09/09/2015.
//  Copyright © 2015 Gavin Williams. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Platinum/Platinum.h>
#import <Platinum/PltUPnPObject.h>
#import <Platinum/PltMediaServerObject.h>
#import <Platinum/PltFileMediaServer.h>
#import <Platinum/PltMediaItem.h>
#import <Platinum/PltService.h>
#import <Platinum/PltTaskManager.h>
#import <Platinum/PltHttpServer.h>
#import <Platinum/PltDidl.h>
#import <Platinum/PltVersion.h>
#import <Platinum/PltMimeType.h>

@class WallBoxDLNAManager;

@protocol WallBoxDLNAManagerDelegate <NSObject>

@required
- (void) wallBoxDLNAManager:(WallBoxDLNAManager *) manager didPlayTrack:(NSURL *) track;
- (void) wallBoxDLNAManager:(WallBoxDLNAManager *) manager didPauseTrack:(NSURL *) track;
- (void) wallBoxDLNAManager:(WallBoxDLNAManager *) manager didResumeTrack:(NSURL *) track;

@end

@interface WallBoxDLNAManager : NSObject <PLT_MediaServerDelegateObject>

@property (nonatomic, strong) NSURL * mediaFolder;
@property (nonatomic, strong) NSMutableArray * files;
@property (nonatomic) bool isRunning;
@property (nonatomic, assign) id<WallBoxDLNAManagerDelegate> delegate;

# pragma mark - Initialisers
- (id) initWithRoot:(NSURL *) root delegate:(id<WallBoxDLNAManagerDelegate>) delegate;

- (NSArray *) getAllFiles;

# pragma mark - Media Server Control Methods

- (void) start;
- (void) stop;

@end
