//
//  WallBoxDLNAManager.m
//  Wall Box
//
//  Created by Gavin Williams on 09/09/2015.
//  Copyright © 2015 Gavin Williams. All rights reserved.
//

#import "WallBoxDLNAManager.h"
#import <Platinum/Neptune.h>

@interface WallBoxDLNAManager ()

@property (nonatomic, retain) PLT_UPnPObject *upnp;

@end

@implementation WallBoxDLNAManager

@synthesize mediaFolder = _mediaFolder, files = _files;

- (id) initWithRoot:(NSURL *)root delegate:(id<WallBoxDLNAManagerDelegate>)delegate {
    
    if(self = [super init]){
        self.delegate = delegate;
        self.mediaFolder = root;
        [self initUPnPServer];
    }
    
    return self;
    
}

- (void) initUPnPServer {
    
    self.upnp = [[PLT_UPnPObject alloc] init];
    
    NSError *error;
    
    self.files = [NSMutableArray arrayWithArray:[[NSFileManager defaultManager] contentsOfDirectoryAtURL:self.mediaFolder includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error]];
    
    if(error){
        NSLog(@"Do Something");
    }
    
    // create server and add ourselves as the delegate
    PLT_MediaServerObject* server = [[PLT_MediaServerObject alloc] init];
    [server setDelegate:self];
    [self.upnp addDevice:server];
    
}

- (NSArray *) getAllFiles {
    
    NSMutableArray * files = [NSMutableArray array];
    
    NSFileManager *fileManager = [[[NSFileManager alloc] init] autorelease];
    NSURL *directoryURL = self.mediaFolder; // URL pointing to the directory you want to browse
    NSArray *keys = [NSArray arrayWithObject:NSURLIsDirectoryKey];
    
    NSDirectoryEnumerator *enumerator = [fileManager
                                         enumeratorAtURL:directoryURL
                                         includingPropertiesForKeys:keys
                                         options:0
                                         errorHandler:^(NSURL *url, NSError *error) {
                                             // Handle the error.
                                             // Return YES if the enumeration should continue after the error.
                                             return YES;
                                         }];
    
    for (NSURL *url in enumerator) {
        NSError *error;
        NSNumber *isDirectory = nil;
        if (! [url getResourceValue:&isDirectory forKey:NSURLIsDirectoryKey error:&error]) {
            // handle error
        }
        else if (! [isDirectory boolValue]) {
            [files addObject:url];
        }
    }
    
    return files;
}

# pragma mark - UPNP Control Methods

- (void) start {
    
    if([self.upnp isRunning] == NO){
        [self.upnp start];
    }
    
}

- (void) stop {
    if([self.upnp isRunning] == YES){
        [self.upnp stop];
    }
}

#pragma mark - UPNP Delegate Methods

- (PLT_MediaObject *) mediaObjectForItem: (NSURL *) file withInfo:(PLT_MediaServerBrowseCapsule *) info {
    
    PLT_MediaObject *mediaObject = NULL;
    PLT_MediaItemResource resource;
    
    NSError *error;
    
    // Is it a file or a folder?
    NSDictionary *fileInfo = [[NSFileManager defaultManager] attributesOfItemAtPath:file.path error:&error];
    
    
    if([fileInfo[NSFileType] isEqualToString:NSFileTypeRegular]){
        
        // Must be a normal file
        mediaObject = new PLT_MediaItem();
        
        mediaObject->m_Title = [[file lastPathComponent] stringByDeletingPathExtension].UTF8String;
        
        resource.m_ProtocolInfo = PLT_ProtocolInfo::GetProtocolInfo(file.path.UTF8String, true, NULL);
        
        resource.m_Size = [fileInfo[NSFileSize] floatValue];
        
        mediaObject->m_ObjectClass.type = PLT_MediaItem::GetUPnPClass(file.path.UTF8String);
        
        NSString *host = [NSString stringWithFormat:@"http://%s:%u", info.context->GetLocalAddress().GetIpAddress().ToString().GetChars(), info.context->GetLocalAddress().GetPort()];
        
        
        resource.m_Uri = [[NSURL URLWithString:host] URLByAppendingPathComponent:[file.path stringByReplacingOccurrencesOfString:self.mediaFolder.path withString:@""]].absoluteString.UTF8String;
        
        mediaObject->m_Resources.Add(resource);
        
    } else {
        // Must be something else
        
        mediaObject = new PLT_MediaContainer();
        
        if([file isEqualTo:_mediaFolder]){
            mediaObject->m_Title = "Root";
        } else {
            mediaObject->m_Title = [[file lastPathComponent] stringByDeletingPathExtension].UTF8String;
        }
        
        NSArray * children = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:file includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&error];
        
        ((PLT_MediaContainer *)mediaObject)->m_ChildrenCount = (int)[children count];
        
        mediaObject->m_ObjectClass.type = "object.container.storageFolder";
    }
    
    if(file == _mediaFolder){
        mediaObject->m_ParentID = "-1";
        mediaObject->m_ObjectID = "0";
    } else {
        
        NSURL *parent = [file URLByDeletingLastPathComponent];
        if([parent isEqualTo:self.mediaFolder]){
            mediaObject->m_ParentID = "0";
        } else {
            mediaObject->m_ParentID = [NSString stringWithFormat:@"0%@", parent.lastPathComponent].UTF8String;
        }
        
        mediaObject->m_ObjectID = [NSString stringWithFormat:@"0%@", [file.path stringByReplacingOccurrencesOfString:self.mediaFolder.path withString:@""]].UTF8String;
        
    }
    
    return mediaObject;
    
}

#pragma mark PLT_MediaServerDelegateObject
- (NPT_Result)onBrowseMetadata:(PLT_MediaServerBrowseCapsule*)info
{
        
    NSMutableString *metadata = [NSMutableString stringWithString:@(didl_header)];
    PLT_MediaObjectReference item;
    
    NSURL *fileURL = [self.mediaFolder URLByAppendingPathComponent:[info.objectId substringFromIndex:1]];
    
    // Should check to see if the file exists here... probably not needed now but will be later!
    item = [self mediaObjectForItem:fileURL withInfo:info];
    
    NPT_String tmp;
    PLT_Didl::ToDidl(*item.AsPointer(), info.filter.UTF8String, tmp);
    
    [metadata appendString:@(tmp.GetChars())];
    
    [metadata appendString:@(didl_footer)];
    
    [info setValue:metadata forArgument:@"Result"];
    [info setValue:@"1" forArgument:@"NumberReturned"];
    [info setValue:@"1" forArgument:@"TotalMatches"];
    
    [info setValue:@"1" forArgument:@"UpdateId"];

    
    return NPT_SUCCESS;
}

- (NPT_Result)onBrowseDirectChildren:(PLT_MediaServerBrowseCapsule*)info
{
            
    NSString * requestedObject = @"";
    
    if(info.objectId.length > 1){
        requestedObject = [info.objectId substringFromIndex:1];
    }
    
    
    NSError *fuckit;
    
    NSArray *files = [[NSFileManager defaultManager] contentsOfDirectoryAtURL:[self.mediaFolder URLByAppendingPathComponent:requestedObject] includingPropertiesForKeys:nil options:NSDirectoryEnumerationSkipsHiddenFiles error:&fuckit];
    
    NSMutableString *directoryList = [NSMutableString stringWithUTF8String:didl_header];
    
    PLT_MediaObjectReference item;
    
    for(NSURL *file in files){
        
        item = [self mediaObjectForItem:file withInfo:info];
        NPT_String tmp;
        PLT_Didl::ToDidl(*item.AsPointer(), info.filter.UTF8String, tmp);
        
        [directoryList appendString:@(tmp.GetChars())];
        
    }
    
    [directoryList appendString:@(didl_footer)];
    
    
    [info setValue:directoryList forArgument:@"Result"];
    [info setValue:[NSString stringWithFormat:@"%lu", (unsigned long)files.count] forArgument:@"NumberReturned"];
    [info setValue:[NSString stringWithFormat:@"%lu", (unsigned long)files.count] forArgument:@"TotalMatches"];
    [info setValue:@"1" forArgument:@"UpdateId"];

    
    
    return NPT_SUCCESS;
}

- (NPT_Result)onSearchContainer:(PLT_MediaServerSearchCapsule*)info
{
    return NPT_FAILURE;
}

- (NPT_Result)onFileRequest:(PLT_MediaServerFileRequestCapsule*)info
{
    
    NSString *fileRequested = [@(info.request->GetUrl().GetPath().GetChars()) stringByRemovingPercentEncoding];

    NSURL *fileToServe = [self.mediaFolder URLByAppendingPathComponent:fileRequested];

    if([self.delegate respondsToSelector:@selector(wallBoxDLNAManager:didPlayTrack:)]){
        [self.delegate wallBoxDLNAManager:self didPlayTrack:fileToServe];
    }

    PLT_HttpServer::ServeFile(*info.request, *info.context, *info.response, fileToServe.path.UTF8String);

    
    return NPT_SUCCESS;
}

@end
