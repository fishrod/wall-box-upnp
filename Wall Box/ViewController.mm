//
//  ViewController.m
//  Wall Box
//
//  Created by Gavin Williams on 04/09/2015.
//  Copyright © 2015 Gavin Williams. All rights reserved.
//

#import "ViewController.h"


@implementation ViewController

- (void) wallBoxDLNAManager:(WallBoxDLNAManager *)manager didChangeTrack:(NSURL *)track {
    NSLog(@"Track: %@", track);
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSURL *directory = [NSURL fileURLWithPath:@"/Users/gavinwilliams/Desktop/Samsung 360 Tour/"];
    
    self.manager = [[WallBoxDLNAManager alloc] initWithRoot:directory delegate:self];
    
    [self.manager start];
    
    NSLog(@"Files: %@", [self.manager getAllFiles]);
    
//
//    upnp = *new PLT_UPnP();
//    
//    PLT_Constants::GetInstance().SetDefaultDeviceLease(NPT_TimeInterval(60.));
//    
//    device = *new PLT_DeviceHostReference(
//        new PLT_FileMediaServer(directory.path.UTF8String,
//                                @"Samsung 360 Media Server".UTF8String,
//                                false,
//                                [[NSUUID UUID] UUIDString].UTF8String,
//                                2080));
//    
//    device->m_ModelDescription = @"Samsung 360 Media Server".UTF8String;
//    device->m_ModelURL = @"http://www.gruve.co.uk".UTF8String;
//    device->m_ModelNumber = @"1.0".UTF8String;
//    device->m_ModelName = @"Samsung 360 Media Server".UTF8String;
//    device->m_Manufacturer = @"Gruve LTD".UTF8String;
//    device->m_ManufacturerURL = @"http://www.gruve.co.uk".UTF8String;
//    
//    upnp.AddDevice(device);
//    
//    upnp.Start();
    
    
    // Do any additional setup after loading the view.
}

- (void)setRepresentedObject:(id)representedObject {
    [super setRepresentedObject:representedObject];

    // Update the view, if already loaded.
}

@end
