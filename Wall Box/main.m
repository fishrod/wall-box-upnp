//
//  main.m
//  Wall Box
//
//  Created by Gavin Williams on 04/09/2015.
//  Copyright © 2015 Gavin Williams. All rights reserved.
//

#import <Cocoa/Cocoa.h>

int main(int argc, const char * argv[]) {
    return NSApplicationMain(argc, argv);
}
