//
//  ViewController.h
//  Wall Box
//
//  Created by Gavin Williams on 04/09/2015.
//  Copyright © 2015 Gavin Williams. All rights reserved.
//

#import <Cocoa/Cocoa.h>
#import "WallBoxDLNAManager.h"

@interface ViewController : NSViewController <WallBoxDLNAManagerDelegate>

@property (nonatomic, strong) WallBoxDLNAManager *manager;

@end

