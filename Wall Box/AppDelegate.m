//
//  AppDelegate.m
//  Wall Box
//
//  Created by Gavin Williams on 04/09/2015.
//  Copyright © 2015 Gavin Williams. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate

- (void)applicationDidFinishLaunching:(NSNotification *)aNotification {
    // Insert code here to initialize your application
}

- (void)applicationWillTerminate:(NSNotification *)aNotification {
    // Insert code here to tear down your application
}

@end
